import urllib.parse

import gnupg
import requests
from django.conf import settings

from django_pwmgr_passbolt.django_pwmgr_passbolt.models import Gpgkeys

LOGIN_URL = "/auth/login.json"
VERIFY_URL = "/auth/verify.json"


class PassboltAPI:
    def __init__(self, new_keys=False, delete_old_keys=False):
        """
        :param delete_old_keys: Set true if old keys need to be deleted
        """
        self.requests_session = requests.Session()

        self.server_url = settings.PASSBOLT_API_SERVER_URL
        self.user_fingerprint = settings.PASSBOLT_API_USER_FINGERPRINT
        self.gpg = gnupg.GPG()
        if delete_old_keys:
            self._delete_old_keys()
        if new_keys:
            self._import_gpg_keys()
        try:
            self.gpg_fingerprint = [
                i
                for i in self.gpg.list_keys()
                if i["fingerprint"] == settings.PASSBOLT_API_USER_FINGERPRINT
            ][0]["fingerprint"]
        except IndexError:
            raise Exception("GPG public key could not be found. Check: gpg --list-keys")

        if settings.PASSBOLT_API_USER_FINGERPRINT not in [
            i["fingerprint"] for i in self.gpg.list_keys(True)
        ]:
            raise Exception(
                "GPG private key could not be found. Check: gpg --list-secret-keys"
            )
        self._login()

    def __enter__(self):
        return self

    def __del__(self):
        self.close_session()

    def __exit__(self, exc_type, exc_value, traceback):
        self.close_session()

    def _delete_old_keys(self):
        for i in self.gpg.list_keys():
            self.gpg.delete_keys(i["fingerprint"], True, passphrase="")
            self.gpg.delete_keys(i["fingerprint"], False)

    def _import_gpg_keys(self):
        if not settings.PASSBOLT_API_USER_PUBKEY_FILE:
            raise ValueError("Missing value for PASSBOLT_API_USER_PUBKEY_FILE in ENV")
        if not settings.PASSBOLT_API_USER_PRIV_KEY_FILE:
            raise ValueError("Missing value for PASSBOLT_API_USER_PRIV_KEY_FILE in ENV")
        self.gpg.import_keys(open(settings.PASSBOLT_API_USER_PUBKEY_FILE, "r").read())
        self.gpg.import_keys(open(settings.PASSBOLT_API_USER_PRIV_KEY_FILE, "r").read())

    def _login(self):
        r = self.requests_session.post(
            self.server_url + LOGIN_URL,
            json={"gpg_auth": {"keyid": self.gpg_fingerprint}},
        )
        encrypted_token = r.headers["X-GPGAuth-User-Auth-Token"]
        encrypted_token = urllib.parse.unquote(encrypted_token)
        encrypted_token = encrypted_token.replace("\+", " ")
        token = self.decrypt(encrypted_token)
        self.requests_session.post(
            self.server_url + LOGIN_URL,
            json={
                "gpg_auth": {"keyid": self.gpg_fingerprint, "user_token_result": token},
            },
        )

    def encrypt(self, text):
        return str(
            self.gpg.encrypt(
                data=text, recipients=self.gpg_fingerprint, always_trust=True
            )
        )

    def decrypt(self, text):
        return str(
            self.gpg.decrypt(
                text,
                always_trust=True,
                passphrase=str(settings.PASSBOLT_API_PASSPHRASE),
            )
        )

    def get_headers(self):
        return {
            "X-CSRF-Token": (
                self.requests_session.cookies["csrfToken"]
                if "csrfToken" in self.requests_session.cookies
                else ""
            )
        }

    def get_server_public_key(self):
        r = self.requests_session.get(self.server_url + VERIFY_URL)
        return r.json()["body"]["fingerprint"], r.json()["body"]["keydata"]

    def get(self, url):
        r = self.requests_session.get(self.server_url + url)
        return r.json()

    def post(self, url, data):
        if "csrfToken" not in self.requests_session.cookies:
            # TODO: Remove warm up (bug: csrf cookie unknown if no data has been fetched during session)
            print("DEBUG: Run warmup")
            warumup = self.get(url="/resources.json?api-version=v2")

        r = self.requests_session.post(
            self.server_url + url, json=data, headers=self.get_headers()
        )
        return r.json()

    def put(self, url, data):
        if "csrfToken" not in self.requests_session.cookies:
            # TODO: Remove warm up (bug: csrf cookie unknown if no data has been fetched during session)
            print("DEBUG: Run warmup")
            warumup = self.get(url="/resources.json?api-version=v2")

        r = self.requests_session.put(
            self.server_url + url, json=data, headers=self.get_headers()
        )
        return r.json()

    def delete(self, url):
        r = self.requests_session.delete(
            self.server_url + url, headers=self.get_headers()
        )
        return r.json()

    def close_session(self):
        self.requests_session.close()

    def import_all_gpg_keys(self):
        for gpg_keys in Gpgkeys.objects.all():
            self.gpg.import_keys(gpg_keys.armored_key)

        return True

    def set_folder_permissions(self, folder_id: str, user_id: str):
        data = dict()
        data["permissions"] = list()
        data["permissions"].append(
            {
                "aco": "Folder",
                "aro": "User",
                "aco_foreign_key": folder_id,
                "aro_foreign_key": user_id,
                "type": 15,
            }
        )

        folder_ressource = self.put(
            url="/share/folder/" + folder_id + ".json?api-version=v2", data=data
        )

        return folder_ressource

    def set_resource_permissions(self, resource_id: str, user_id: str):
        current_resource = self.get(
            url="/secrets/resource/" + resource_id + ".json?api-version=v2"
        )
        cleartext_password = self.decrypt(current_resource["body"]["data"])

        data = dict()
        data["permissions"] = list()
        data["permissions"].append(
            {
                "aco": "Resource",
                "aro": "User",
                "aco_foreign_key": resource_id,
                "aro_foreign_key": user_id,
                "type": 15,
            }
        )

        secret_encrypted_new = str(
            self.gpg.encrypt(
                data=cleartext_password,
                recipients=Gpgkeys.objects.get(user_id=user_id).fingerprint,
                always_trust=True,
            )
        )

        data["secrets"] = list()
        data["secrets"].append(
            {
                "resource_id": resource_id,
                "user_id": user_id,
                "data": secret_encrypted_new,
            }
        )

        ressource = self.put(
            url="/share/resource/" + resource_id + ".json?api-version=v2", data=data
        )

        return ressource
