from django.db import models


class AccountRecoveryOrganizationPolicies(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    public_key_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    policy = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()
    deleted = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_organization_policies"


class AccountRecoveryOrganizationPublicKeys(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    armored_key = models.TextField()
    fingerprint = models.CharField(
        unique=True, max_length=40, db_collation="ascii_general_ci"
    )
    created = models.DateTimeField()
    modified = models.DateTimeField()
    deleted = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_organization_public_keys"


class AccountRecoveryPrivateKeyPasswords(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    recipient_fingerprint = models.CharField(
        max_length=40, db_collation="ascii_general_ci"
    )
    recipient_foreign_model = models.CharField(max_length=128)
    private_key_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    data = models.TextField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_private_key_passwords"


class AccountRecoveryPrivateKeys(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    data = models.TextField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_private_keys"


class AccountRecoveryRequests(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    armored_key = models.TextField(blank=True, null=True)
    fingerprint = models.CharField(
        max_length=40, db_collation="ascii_general_ci", blank=True, null=True
    )
    authentication_token_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci"
    )
    status = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_requests"


class AccountRecoveryResponses(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    account_recovery_request_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci"
    )
    responder_foreign_key = models.CharField(
        max_length=36, db_collation="ascii_general_ci"
    )
    responder_foreign_model = models.CharField(max_length=128)
    data = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_responses"


class AccountRecoveryUserSettings(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    status = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "account_recovery_user_settings"


class AccountSettings(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    property_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    property = models.CharField(max_length=256)
    value = models.TextField()
    created = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "account_settings"


class ActionLogs(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    action_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    context = models.CharField(max_length=255)
    status = models.IntegerField()
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "action_logs"


class Actions(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(unique=True, max_length=100)

    class Meta:
        managed = False
        db_table = "actions"


class AuthenticationTokens(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    token = models.CharField(max_length=36, db_collation="ascii_general_ci")
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    active = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    type = models.CharField(max_length=16, db_collation="ascii_general_ci")
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "authentication_tokens"


class Avatars(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    data = models.TextField(blank=True, null=True)
    profile_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "avatars"


class Comments(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    parent_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    foreign_model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    content = models.CharField(max_length=256)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "comments"


class DirectoryEntries(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    foreign_model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    foreign_key = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    directory_name = models.CharField(max_length=256)
    directory_created = models.DateTimeField(blank=True, null=True)
    directory_modified = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "directory_entries"


class DirectoryIgnore(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    foreign_model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "directory_ignore"


class DirectoryRelations(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    parent_key = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    child_key = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "directory_relations"


class DirectoryReports(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    parent_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    status = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "directory_reports"


class DirectoryReportsItems(models.Model):
    report_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    status = models.CharField(max_length=36, db_collation="ascii_general_ci")
    model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    action = models.CharField(max_length=36)
    data = models.TextField(blank=True, null=True)
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "directory_reports_items"


class EmailQueue(models.Model):
    email = models.CharField(max_length=129)
    from_name = models.CharField(max_length=255, blank=True, null=True)
    from_email = models.CharField(max_length=255, blank=True, null=True)
    subject = models.CharField(max_length=255)
    config = models.CharField(max_length=30)
    template = models.CharField(max_length=100)
    layout = models.CharField(max_length=50)
    theme = models.CharField(max_length=50)
    format = models.CharField(max_length=5)
    template_vars = models.TextField()
    headers = models.TextField(blank=True, null=True)
    sent = models.IntegerField()
    locked = models.IntegerField()
    send_tries = models.IntegerField()
    send_at = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField()
    modified = models.DateTimeField(blank=True, null=True)
    attachments = models.TextField(blank=True, null=True)
    error = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "email_queue"


class EntitiesHistory(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    action_log_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    foreign_model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    crud = models.CharField(max_length=1, db_collation="ascii_general_ci")
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "entities_history"


class Favorites(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    foreign_model = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "favorites"


class Folders(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(max_length=64)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "folders"


class FoldersHistory(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    folder_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    name = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = "folders_history"


class FoldersRelations(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    foreign_model = models.CharField(max_length=30, db_collation="ascii_general_ci")
    foreign_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    folder_parent_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "folders_relations"


class FoldersRelationsHistory(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    foreign_model = models.CharField(max_length=30, db_collation="ascii_general_ci")
    foreign_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    folder_parent_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )

    class Meta:
        managed = False
        db_table = "folders_relations_history"


class Gpgkeys(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    armored_key = models.TextField()
    bits = models.IntegerField(blank=True, null=True)
    uid = models.CharField(max_length=128)
    key_id = models.CharField(max_length=16)
    fingerprint = models.CharField(max_length=51)
    type = models.CharField(max_length=16, blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    key_created = models.DateTimeField(blank=True, null=True)
    deleted = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "gpgkeys"


class Groups(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(max_length=255, blank=True, null=True)
    deleted = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "groups"


class GroupsUsers(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    group_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    user_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    is_admin = models.IntegerField()
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "groups_users"


class OrganizationSettings(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    property_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    property = models.CharField(max_length=256)
    value = models.TextField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "organization_settings"


class Permissions(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    aco = models.CharField(max_length=30, db_collation="ascii_general_ci")
    aco_foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    aro = models.CharField(max_length=30, db_collation="ascii_general_ci")
    aro_foreign_key = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    type = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "permissions"


class PermissionsHistory(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    aco = models.CharField(max_length=30, db_collation="ascii_general_ci")
    aco_foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    aro = models.CharField(max_length=30, db_collation="ascii_general_ci")
    aro_foreign_key = models.CharField(max_length=36, db_collation="ascii_general_ci")
    type = models.IntegerField()

    class Meta:
        managed = False
        db_table = "permissions_history"


class Phinxlog(models.Model):
    version = models.BigIntegerField(primary_key=True)
    migration_name = models.CharField(max_length=100, blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    breakpoint = models.IntegerField()

    class Meta:
        managed = False
        db_table = "phinxlog"


class Profiles(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "profiles"


class ResourceTypes(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    slug = models.CharField(unique=True, max_length=64)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=255, blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "resource_types"


class Resources(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255, blank=True, null=True)
    uri = models.CharField(max_length=1024, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    deleted = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    modified_by = models.CharField(max_length=36, db_collation="ascii_general_ci")
    resource_type_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )

    class Meta:
        managed = False
        db_table = "resources"


class ResourcesTags(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    resource_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    tag_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    user_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci", blank=True, null=True
    )
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "resources_tags"


class Roles(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(unique=True, max_length=50)
    description = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "roles"


class SecretAccesses(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    resource_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    secret_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "secret_accesses"


class Secrets(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    resource_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    data = models.TextField()
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "secrets"


class SecretsHistory(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    resource_id = models.CharField(max_length=36, db_collation="ascii_general_ci")

    class Meta:
        managed = False
        db_table = "secrets_history"


class Tags(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    slug = models.CharField(max_length=128)
    is_shared = models.IntegerField()

    class Meta:
        managed = False
        db_table = "tags"


class Transfers(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    user_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    authentication_token_id = models.CharField(
        max_length=36, db_collation="ascii_general_ci"
    )
    current_page = models.SmallIntegerField()
    total_pages = models.SmallIntegerField()
    status = models.CharField(max_length=16, db_collation="ascii_general_ci")
    hash = models.CharField(max_length=128, db_collation="ascii_general_ci")
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "transfers"


class UserAgents(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "user_agents"


class Users(models.Model):
    id = models.CharField(
        primary_key=True, max_length=36, db_collation="ascii_general_ci"
    )
    role_id = models.CharField(max_length=36, db_collation="ascii_general_ci")
    username = models.CharField(max_length=255)
    active = models.IntegerField()
    deleted = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "users"
