from django.core.management.base import BaseCommand

from django_pwmgr_passbolt.django_pwmgr_passbolt.tasks import (
    pwmgr_passbolt_optimize_structure,
)


class Command(BaseCommand):
    help = "Run task pwmgr_passbolt_optimize_structure"

    def handle(self, *args, **options):
        pwmgr_passbolt_optimize_structure()
        self.stdout.write(self.style.SUCCESS("Successfully finished."))
