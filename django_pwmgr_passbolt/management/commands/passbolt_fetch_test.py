from django.core.management.base import BaseCommand

from django_pwmgr_passbolt.django_pwmgr_passbolt.func import *


class Command(BaseCommand):
    help = "Test Passbolt"

    def handle(self, *args, **options):
        print("======================== TEST1 ========================")
        fetch_test()
        print("======================== TEST2 ========================")
        fetch_test2()
        self.stdout.write(self.style.SUCCESS("Successfully finished."))
