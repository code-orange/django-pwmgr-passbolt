from django.core.management.base import BaseCommand

from django_pwmgr_passbolt.django_pwmgr_passbolt.tasks import (
    pwmgr_passbolt_admin_sharing,
)


class Command(BaseCommand):
    help = "Run task pwmgr_passbolt_admin_sharing"

    def handle(self, *args, **options):
        pwmgr_passbolt_admin_sharing()
        self.stdout.write(self.style.SUCCESS("Successfully finished."))
