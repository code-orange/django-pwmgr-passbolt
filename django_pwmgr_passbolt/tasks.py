from celery import shared_task

from django_pwmgr_models.django_pwmgr_models.models import *
from django_pwmgr_passbolt.django_pwmgr_passbolt.func import *


@shared_task(name="pwmgr_passbolt_optimize_structure")
def pwmgr_passbolt_optimize_structure():
    folders_per_org = (
        "SRVFARM",
        "Backup",
        "Internet",
        "Webhosting",
        "Telephone",
        "Automation Devices",
        "Printers",
        "Network Equipment",
        "External Websites",
        "E-Mail",
        "Databases",
        "Network Security",
        "Microsoft 365",
    )

    passbolt = passbolt_connect()

    for tenant in PwmgrTenantSettings.objects.filter(customer__org_tag__isnull=False):
        # Search root folder
        root_folder = find_root_folder(tenant.customer.org_tag)

        print("DEBUG: " + tenant.customer.org_tag + " matches selection")

        if root_folder is False:
            # create root folder
            create_root_folder(passbolt, tenant.customer.org_tag)
            root_folder = find_root_folder(tenant.customer.org_tag)

        for folder_per_org in folders_per_org:
            sub_folder = find_sub_folder(root_folder, folder_per_org)

            if sub_folder is False:
                # create sub folder
                create_sub_folder(passbolt, root_folder, folder_per_org)
                sub_folder = find_sub_folder(root_folder, folder_per_org)

    return


@shared_task(name="pwmgr_passbolt_admin_sharing")
def pwmgr_passbolt_admin_sharing():
    passbolt = passbolt_connect()

    userids_owner_access = (
        "a0fddce2-fd62-49d9-add5-0ab4d8e43546",
        "ac8baf69-fd05-43ab-8677-e2585983f758",
        "bf21993a-4376-438d-afd6-58be1f83d173",
        "fabb9e8f-a313-4e81-99b7-5f9e98d40475",
    )

    for folder in Folders.objects.all():
        # check if top-level
        try:
            current_relation = FoldersRelations.objects.get(
                foreign_model="Folder",
                foreign_id=folder.id,
                user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
                folder_parent_id__isnull=True,
            )
        except FoldersRelations.DoesNotExist:
            continue

        print(
            "DEBUG: Folder "
            + folder.name
            + " with ID "
            + current_relation.id
            + " matches selection"
        )

        for userid_owner_access in userids_owner_access:
            passbolt.set_folder_permissions(folder.id, userid_owner_access)

        # next level dirs
        # TODO: allow more than two folders down
        sub_folders = FoldersRelations.objects.filter(
            foreign_model="Folder",
            user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
            folder_parent_id=folder.id,
        )

        for sub_folder in sub_folders:
            for userid_owner_access in userids_owner_access:
                passbolt.set_folder_permissions(
                    sub_folder.foreign_id, userid_owner_access
                )

        # share ressources
        passbolt.import_all_gpg_keys()

        root_resources = FoldersRelations.objects.filter(
            foreign_model="Resource",
            user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
            folder_parent_id=folder.id,
        )

        for root_resource in root_resources:
            for userid_owner_access in userids_owner_access:
                passbolt.set_resource_permissions(
                    root_resource.foreign_id, userid_owner_access
                )

        for sub_folder in sub_folders:
            sub_resources = FoldersRelations.objects.filter(
                foreign_model="Resource",
                user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
                folder_parent_id=sub_folder.foreign_id,
            )

            for sub_resource in sub_resources:
                for userid_owner_access in userids_owner_access:
                    passbolt.set_resource_permissions(
                        sub_resource.foreign_id, userid_owner_access
                    )

    return
