from django_pwmgr_passbolt.django_pwmgr_passbolt.api_helper import PassboltAPI
from django_pwmgr_passbolt.django_pwmgr_passbolt.models import *


def passbolt_connect():
    print("DEBUG: Run passbolt_connect")
    passbolt = PassboltAPI()
    print("DEBUG: Run passbolt_connect - finished")
    return passbolt


def fetch_test():
    passbolt = PassboltAPI()
    r = passbolt.get(url="/resources.json?api-version=v2")


def fetch_test2():
    passbolt = PassboltAPI()
    r = passbolt.get(
        url="/secrets/resource/1207f936-b77e-4a49-a994-340c5b7a9b4a.json?api-version=v2"
    )
    cleartext_password = passbolt.decrypt(r["body"]["data"])


def find_root_folder(org_tag: str):
    # Find all folders matching tag
    folders_matching_tag = Folders.objects.filter(name=org_tag)

    for folder_matching_tag in folders_matching_tag:
        try:
            current_relation = FoldersRelations.objects.get(
                foreign_model="Folder",
                foreign_id=folder_matching_tag.id,
                user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
                folder_parent_id__isnull=True,
            )
        except FoldersRelations.DoesNotExist:
            continue

        return folder_matching_tag

    return False


def find_sub_folder(folder: Folders, name: str):
    # Find all folders matching tag
    folders_matching_name = Folders.objects.filter(name=name)

    for folder_matching_name in folders_matching_name:
        try:
            current_relation = FoldersRelations.objects.get(
                foreign_model="Folder",
                foreign_id=folder_matching_name.id,
                user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
                folder_parent_id=folder.id,
            )
        except FoldersRelations.DoesNotExist:
            continue

        return folder_matching_name

    return False


def create_root_folder(passbolt, org_tag: str):
    print("DEBUG: Run create_root_folder for " + org_tag)

    folder_data = dict()
    # folder_parent_id as None makes it to-level
    folder_data["folder_parent_id"] = None
    folder_data["name"] = org_tag

    return passbolt.post(
        url="/folders.json?api-version=v2&contain[permission]=1", data=folder_data
    )


def create_sub_folder(passbolt, folder: Folders, name: str):
    print("DEBUG: Run create_sub_folder for " + name)

    folder_data = dict()
    # folder_parent_id as None makes it to-level
    folder_data["folder_parent_id"] = folder.id
    folder_data["name"] = name

    return passbolt.post(
        url="/folders.json?api-version=v2&contain[permission]=1", data=folder_data
    )


def write_credentials_to_folder(passbolt, folder: Folders, cred_data, cred_id=None):
    cred_data["secrets"] = list()

    if cred_id is not None:
        # current_resource = passbolt.get(url='/resources/' + cred_id + '.json?api-version=v2')
        attached_folder_relation = FoldersRelations.objects.get(
            foreign_id=cred_id,
            foreign_model="Resource",
            user_id="ac8baf69-fd05-43ab-8677-e2585983f758",
        )

        # Check credentials are in the correct folder
        # if not Folders.objects.get(id=attached_folder_relation.folder_parent_id).id == folder.id:
        #     return False

        # Check who has access to resource
        attached_users = Permissions.objects.filter(
            aco="Resource",
            aco_foreign_key=cred_id,
            aro="User",
        )

        if "password_cleartext" in cred_data:
            for attached_user in attached_users:
                secret_encrypted_new = str(
                    passbolt.gpg.encrypt(
                        data=cred_data["password_cleartext"],
                        recipients=Gpgkeys.objects.get(
                            user_id=attached_user.aro_foreign_key
                        ).fingerprint,
                        always_trust=True,
                    )
                )

                cred_data["secrets"].append(
                    {
                        "user_id": attached_user.aro_foreign_key,
                        "data": secret_encrypted_new,
                    }
                )
    else:
        if "password_cleartext" in cred_data:
            cred_data["secrets"].append(
                {
                    "data": passbolt.encrypt(cred_data["password_cleartext"]),
                }
            )

    if "password_cleartext" not in cred_data:
        del cred_data["secrets"]

    if "password_cleartext" in cred_data:
        del cred_data["password_cleartext"]

    # Create or update
    if cred_id is None:
        api_response = passbolt.post(
            url="/resources.json?api-version=v2&contain[permission]=1", data=cred_data
        )
    else:
        api_response = passbolt.put(
            url="/resources/" + cred_id + ".json?api-version=v2", data=cred_data
        )
        move_response = passbolt.put(
            url="/move/resource/" + cred_id + ".json?api-version=v2",
            data={"id": cred_id, "folder_parent_id": folder.id},
        )

    return api_response


def read_credential(passbolt, cred_id: str):
    r = passbolt.get(url="/secrets/resource/" + cred_id + ".json?api-version=v2")

    return passbolt.decrypt(r["body"]["data"])
